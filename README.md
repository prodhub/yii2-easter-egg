Easter Egg Action for Yii2
==========================
For individual use

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist platx/yii2-easter-egg "*"
```

or add

```
"platx/yii2-easter-egg": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your controller by adding following code  :

```php
public function actions()
{
    return [
        'easter-egg' => [
            'class' => 'platx\easteregg\EasterEggAction',
        ],
    ];
}
```

After this to view easter egg on site go to:
```
/controller/easter-egg?token=powered
```